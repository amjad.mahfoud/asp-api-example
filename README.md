Restaurant Restful API documentation:

This API is bult to performe basic CRUD (Create, Update, Delete) operations 
on a rtesturant model defined as follows:
    Id, Name, Address, Email, DateRegistered

1- General API routes description
Route                HTTP method  Description
===================================================================================================
api/Restaurants         GET       Returns a list of all restaurants as in 2.1
api/Restaurants/{id}    GET       Returns a specific restaurant with the id of {id} as in 2.2
api/Restaurants/{id}    PUT       Deletes a specific restaurant with the id of {id} as in 2.3
api/Restaurants         POST      Create a new restaurant as in 2.4
api/Restaurants/{id}    DELETE    Delets a specific restaurant with the id of {id} as in 2.5


2- More API details:

2.1- GET api/Restaurants
    HTTP method: GET
    URI Parameters: None
    Body Parameters: None
    Response Information: Returns a collection of Restaurants
    Response Sample in "application/json, text/json" format"
        [
        {
            "Id": 1,
            "Name": "sample string 2",
            "Address": "sample string 3",
            "Email": "sample string 4",
            "DateRegistered": "2019-01-08T12:47:55.1633533+02:00"
        },
        {
            "Id": 1,
            "Name": "sample string 2",
            "Address": "sample string 3",
            "Email": "sample string 4",
            "DateRegistered": "2019-01-08T12:47:55.1633533+02:00"
        }
        ]

2.2- GET api/Restaurants/{id}
    HTTP method: GET
    URI Parameters: id, type: integer, required
    Body Parameters: None
    Response Information: Returns a Restaurant and 200 OK or HTTP not found 404
    Response Sample in "application/json, text/json" format"
        {
        "Id": 1,
        "Name": "sample string 2",
        "Address": "sample string 3",
        "Email": "sample string 4",
        "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }

2.3- PUT api/Restaurants/{id}
    HTTP method: PUT
    URI Parameters: id, type: integer, required
    Body Parameters: 
        Id, integer, Required	
        Name, string, Required, String length: inclusive between 6 and 100
        Address, string, Required, String length: inclusive between 6 and 100
        Email, string, Required, EmailAddress
        DateRegistered, Date
        Example:
        {
            "Id": 1,
            "Name": "sample string 2",
            "Address": "sample string 3",
            "Email": "sample string 4",
            "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }

    Response Information: Updates and returns a Restaurant and 200 OK or HTTP not found 404
    Response Sample in "application/json, text/json" format"
        {
        "Id": 1,
        "Name": "sample string 2",
        "Address": "sample string 3",
        "Email": "sample string 4",
        "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }

2.4- POST api/Restaurants
    HTTP method: POST
    URI Parameters: None
    Body Parameters: 
        Name, string, Required, String length: inclusive between 6 and 100
        Address, string, Required, String length: inclusive between 6 and 100
        Email, string, Required, EmailAddress
        DateRegistered, Date
        Example:
        {
            "Name": "sample string 2",
            "Address": "sample string 3",
            "Email": "sample string 4",
            "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }

    Response Information: Creates and returns a Restaurant and 200 OK
    Response Sample in "application/json, text/json" format"
        {
        "Id": 1,
        "Name": "sample string 2",
        "Address": "sample string 3",
        "Email": "sample string 4",
        "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }


2.5- DELETE api/Restaurants/{id}
    HTTP method: DELETE
    URI Parameters: URI Parameters: id, type: integer, required
    Body Parameters: None.
    Response Information: Deletes and returns a Restaurant and 200 OK or 404 not found
    Response Sample in "application/json, text/json" format"
        {
        "Id": 1,
        "Name": "sample string 2",
        "Address": "sample string 3",
        "Email": "sample string 4",
        "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
        }


3- Further Improvements:
    1. Add JWT authintication
    2. Add the appility of paging the Response (ex: api/Restaurants?page=1&per_page=10)
        Response will have the following format:
        {
            "data": [
                {
                    "Id": 1,
                    "Name": "sample string 2",
                    "Address": "sample string 3",
                    "Email": "sample string 4",
                    "DateRegistered": "2019-01-08T12:52:13.287721+02:00"
                }
            ],
            "links": {
                "first": "api/Restaurants?page=1",
                "last": "api/Restaurants?page=3",
                "prev": null,
                "next": "api/Restaurants?page=2"
            },
            "meta": {
                "current_page": 1,
                "from": 3,
                "last_page": 3,                
                "per_page": 10,
                "total": 25
            }
        }
    3. Add the appility of sorting the response with deffirent parameters (ex: api/Restaurants?sort_by=name&sort_type=Desc)